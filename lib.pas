function getTextFromW3strTableStr (s : AnsiString) : AnsiString;
var
  n : LongWord = 1;
  r : AnsiString = '';
  c : word = 0;
begin
  repeat
    if s[n] = '|'
      then c := c + 1;
    n := n + 1;
  until c = 3;
  repeat
    r := r + s[n];
    n := n + 1;
  until (n = length(s)+1);
  //writeln('"'+ r+'"');
  getTextFromW3strTableStr := r;
end;

function removeSpaceFromStr (s : AnsiString): AnsiString;
var
  r : AnsiString = '';
  n : LongWord = 0;
begin
  repeat
    n := n + 1;
    if s[n] <> #32
      then r := r + s[n];
  until (n = length(s));
  removeSpaceFromStr := r;
end;
function getIDFromW3strTableStr(s: AnsiString) : LongWord;
var
  n : LongWord = 1;
  r : AnsiString = '';
  e : boolean = false;
begin
  repeat
    if s[n] = '|'
      then e := true
      else r := r + s[n];
    n := n + 1;
  until e;
  //writeln('"'+ r+'"');
  getIDFromW3strTableStr := StrToInt(removeSpaceFromStr(r));
end;

function getStringFromId (id : LongWord; path : ansistring) : AnsiString;
  var
    f : text;
    s : AnsiString;
    e : boolean = false;
    r : AnsiString = '';
  begin
    assign(f, path);
    reset(f);
    repeat
      readln(f, s);
      if s[1] <> ';'
        then begin
          if getIDFromW3strTableStr(s) = id
            then begin
              r := getTextFromW3strTableStr(s);
              e := true;
              {writeln (getIDFromW3strTableStr(s),' = ', id);
              writeln (getTextFromW3strTableStr(s));}
            end;
        end;
      {read(f, ch);
      writeln(ord(ch));}
    until EOF(f) or e;
    close(f);
    if e
      then getStringFromId := r
      else getStringFromId := '';
  end;
procedure arrayAdd(var arr : Aas; s : Ansistring);
var
  l : Longword;
begin
  l := length(arr);
  setlength(arr, l + 1);
  arr[l] := s;
end;
procedure ArrayloadFromTxt (var arr : Aas; path : AnsiString);
var
  f : text;
  s : AnsiString;
begin
  assign(f,path);
  reset(f);
  repeat
    readln(f, s);
    arrayAdd(arr, s);
  until EOF(f) or (length(arr) > 4000000000);
  close(f);
end;

procedure ArraySaveToTxt (arr : Aas; path : Ansistring);
var
  f : text;
  i : Longword;
begin
  assign(f, path);
  rewrite(f);
  i := 0;
  if length(arr) > 0
    then
      repeat
      writeln(f, arr[i]);
      i := i + 1;
    until i = length(arr);
  close(f);
end;
